package br.com.hoteis.api.model;

public class GitResp {

	private String commit;
	
	private String branch;
	
	private String buildTime;
	
	private String requestTime;
	
	public GitResp() {
	}
	
	public GitResp(String commit, String branch, String buildTime, String requestTime) {
		super();
		this.commit = commit;
		this.branch = branch;
		this.buildTime = buildTime;
		this.requestTime = requestTime;
	}

	public String getCommit() {
		return commit;
	}

	public void setCommit(String commit) {
		this.commit = commit;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getBuildTime() {
		return buildTime;
	}

	public void setBuildTime(String buildTime) {
		this.buildTime = buildTime;
	}

	public String getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(String requestTime) {
		this.requestTime = requestTime;
	}
	
	
	
}
