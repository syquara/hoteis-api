package br.com.hoteis.api.model;

public class MessageSourceModel {

	private String code;

	private String message;

	public MessageSourceModel() {
	}
	
	public MessageSourceModel(String code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	
	
}
