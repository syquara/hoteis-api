package br.com.hoteis.api.model;

import org.springframework.http.HttpStatus;

public class Erro {

	private HttpStatus status;
	
	private Integer codigo;
	
	private String message;

	public Erro() {
		super();
	}
	
	public Erro(HttpStatus status) {
		super();
		this.status = status;
	}

	public Erro(HttpStatus status, Integer codigo, String message) {
		super();
		this.status = status;
		this.codigo = codigo;
		this.message = message;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

		
	
}
