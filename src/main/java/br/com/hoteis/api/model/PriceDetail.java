package br.com.hoteis.api.model;

public class PriceDetail {

	private Double pricePerDayAdult;
	
	private Double pricePerDayChild;

	public PriceDetail() {
	}
	
	public PriceDetail(Double pricePerDayAdult, Double pricePerDayChild) {
		super();
		this.pricePerDayAdult = pricePerDayAdult;
		this.pricePerDayChild = pricePerDayChild;
	}

	public Double getPricePerDayAdult() {
		return pricePerDayAdult;
	}

	public void setPricePerDayAdult(Double pricePerDayAdult) {
		this.pricePerDayAdult = pricePerDayAdult;
	}

	public Double getPricePerDayChild() {
		return pricePerDayChild;
	}

	public void setPricePerDayChild(Double pricePerDayChild) {
		this.pricePerDayChild = pricePerDayChild;
	}
	
	
}
