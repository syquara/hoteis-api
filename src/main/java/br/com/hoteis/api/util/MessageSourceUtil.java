package br.com.hoteis.api.util;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import br.com.hoteis.api.enums.ResponseAndExceptionEnum;
import br.com.hoteis.api.model.MessageSourceModel;

@Component
public class MessageSourceUtil {
	
	private static MessageSource messageSource;

	public MessageSourceUtil(MessageSource messageSource) {
		MessageSourceUtil.messageSource = messageSource;
	}
	
	public static MessageSourceModel getProperties(ResponseAndExceptionEnum value) {
		MessageSourceModel message = new MessageSourceModel();
		message.setCode(String.valueOf(value.getCode()));
		message.setMessage(messageSource.getMessage(value.getMessageKey(), null, Locale.getDefault()));
		return message;
	}
	
	public static MessageSourceModel getProperties(ResponseAndExceptionEnum value, String... params) {
		MessageSourceModel message = new MessageSourceModel();
		message.setCode(String.valueOf(value.getCode()));
		message.setMessage(messageSource.getMessage(value.getMessageKey(), params, Locale.getDefault()));
		return message;
	}
}
