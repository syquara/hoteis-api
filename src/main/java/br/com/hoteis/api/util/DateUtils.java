package br.com.hoteis.api.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import org.springframework.util.StringUtils;

import br.com.hoteis.api.exception.FormatoDataInvalidoException;

public final class DateUtils {

	private static final String DATE_PATTERN_DDMMYYYY = "ddMMyyyy";

	public static long calculaDias(String dtIni, String dtFim) {
		LocalDate datInicial = isValidDate(dtIni, DATE_PATTERN_DDMMYYYY);
		LocalDate datFinal = isValidDate(dtFim, DATE_PATTERN_DDMMYYYY);
		long dias = ChronoUnit.DAYS.between(datInicial, datFinal);
		return dias;
	}

	public static LocalDate isValidDate(String data, String pattern) {
		try {
			return LocalDate.parse(data, DateTimeFormatter.ofPattern(pattern));
		} catch (Exception e) {
			throw new FormatoDataInvalidoException();
		}

	}

	public static String toString(LocalDateTime dateTime, String pattern) {
		if (dateTime == null)
			return null;

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(pattern);
		return dateTime.format(dtf);
	}

	public static String dateTimeNowToString(String pattern, ZoneId zone) {
		if (!StringUtils.hasText(pattern) || null == zone) {
			return null;
		}
		return toString(LocalDateTime.now(zone), pattern);
	}

}
