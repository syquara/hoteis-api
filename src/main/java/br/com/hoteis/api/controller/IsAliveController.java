package br.com.hoteis.api.controller;

import java.time.ZoneId;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.hoteis.api.enums.ResponseAndExceptionEnum;
import br.com.hoteis.api.model.GitResp;
import br.com.hoteis.api.model.MessageSourceModel;
import br.com.hoteis.api.util.DateUtils;
import br.com.hoteis.api.util.MessageSourceUtil;

@RestController
public class IsAliveController {

	@Value("${git.commit.id.abbrev}")
	private String commitId;

	@Value("${git.build.time}")
	private String buildTime;

	@Value("${git.branch}")
	private String branch;
	
	@Value("${application.name}")
	private String apiName;
	
	@GetMapping("/isAlive")
	public ResponseEntity<?> isALive() {
		String now = DateUtils.dateTimeNowToString("dd/MM/yyyy HH:mm:ss", ZoneId.of("America/Sao_Paulo"));
		MessageSourceModel requestTime = MessageSourceUtil.getProperties(ResponseAndExceptionEnum.CONSULTA_REALIZADA, now);
		return new ResponseEntity<GitResp>(new GitResp(commitId, branch, buildTime, requestTime.getMessage()), HttpStatus.OK);
	}

}
