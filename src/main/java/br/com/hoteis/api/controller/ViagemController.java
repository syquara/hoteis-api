package br.com.hoteis.api.controller;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.hoteis.api.model.HotelCidade;
import br.com.hoteis.api.service.ViagemService;

@RestController
@RequestMapping("/viagem")
public class ViagemController {

	@Autowired
	private ViagemService viagemService;

	@GetMapping(value = "/valorHoteisCidade/{idCidade}", produces = "application/json" )
	public ResponseEntity<?> valorHoteisCidade (@PathVariable ("idCidade") String idCidade, 
												@PathParam ("checkin") String checkin,
												@PathParam ("checkout") String checkout,
												@PathParam ("quantidadeAdultos") Integer quantidadeAdultos,
												@PathParam ("quantidadeCriancas") Integer quantidadeCriancas) {
		
		List<HotelCidade> valorHoteisCidade = viagemService.valorHoteisCidade(idCidade, checkin, checkout, quantidadeAdultos, quantidadeCriancas);
		return new ResponseEntity<List<HotelCidade>>(valorHoteisCidade, HttpStatus.OK);
	}
	
	@GetMapping(value = "/valorHoteis/{idHotel}", produces = "application/json" )
	public ResponseEntity<?> valorHoteis (@PathVariable ("idHotel") String idHotel, 
								  		  @PathParam ("checkin") String checkin,
										  @PathParam ("checkout") String checkout,
										  @PathParam ("quantidadeAdultos") Integer quantidadeAdultos,
										  @PathParam ("quantidadeCriancas") Integer quantidadeCriancas) {
		
		List<HotelCidade> valorHoteisCidade = viagemService.valorHotel(idHotel, checkin, checkout, quantidadeAdultos, quantidadeCriancas);
		return new ResponseEntity<List<HotelCidade>>(valorHoteisCidade, HttpStatus.OK);
	}
	
	

}
