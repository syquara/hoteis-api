package br.com.hoteis.api.enums;

public enum ResponseAndExceptionEnum {

		// O range 1 -> 100 reservado para excecoes da api
	
		ERRO_SERVER 			(1, "erro.server"				),
		ERRO_INTERNO 			(2, "erro.erroInterno"			),
		CAMPOS_OBRIGATORIOS 	(3, "erro.camposObrigatorios"	),
		FORMATO_DATA_INVALIDO	(4, "erro.formatoDataInvalido"	),
		SEM_RESULTADOS			(5, "erro.semResulatados"		),
		
		// O range a partir do 101 destinado a responses
		
		CONSULTA_REALIZADA					(101, "response.consultarealizada"),
		
		
		;
		
		private final Integer code;
		private final String messageKey;
		
		private ResponseAndExceptionEnum (final Integer code, final String messageKey) {
			this.code = code;
			this.messageKey = messageKey;
		}

		public Integer getCode() {
			return code;
		}

		public String getMessageKey() {
			return messageKey;
		}
	
	
}
