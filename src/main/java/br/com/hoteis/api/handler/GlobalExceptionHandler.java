package br.com.hoteis.api.handler;

import java.util.Locale;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.hoteis.api.enums.ResponseAndExceptionEnum;
import br.com.hoteis.api.exception.CamposObrigatoriosException;
import br.com.hoteis.api.exception.ErroInternoException;
import br.com.hoteis.api.exception.FormatoDataInvalidoException;
import br.com.hoteis.api.exception.SemResultadosException;
import br.com.hoteis.api.model.Erro;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
	
	Logger logger = LoggerFactory.getLogger(getClass());
	
	private final MessageSource messageSource;

	public GlobalExceptionHandler(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@ExceptionHandler(Exception.class)
	private ResponseEntity<Object> handleGeneral(Exception e, WebRequest request) {
		return handleException(ResponseAndExceptionEnum.ERRO_SERVER, new Object[] { e.getMessage() }, e, request, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(ErroInternoException.class)
	private ResponseEntity<Object> erroInterno(ErroInternoException e, WebRequest request) {
		return handleException(ResponseAndExceptionEnum.ERRO_INTERNO, new Object[] { e.getMessage() }, e, request, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(CamposObrigatoriosException.class)
	private ResponseEntity<Object> camposObrigatorios(CamposObrigatoriosException e, WebRequest request) {
		return handleException(ResponseAndExceptionEnum.CAMPOS_OBRIGATORIOS, new Object[] { e.getMessage() }, e, request, HttpStatus.PARTIAL_CONTENT);
	}
	
	@ExceptionHandler(FormatoDataInvalidoException.class)
	private ResponseEntity<Object> formatoDataInvalido(FormatoDataInvalidoException e, WebRequest request) {
		return handleException(ResponseAndExceptionEnum.FORMATO_DATA_INVALIDO, new Object[] { e.getMessage() }, e, request, HttpStatus.PARTIAL_CONTENT);
	}
	
	@ExceptionHandler(SemResultadosException.class)
	private ResponseEntity<Object> SemResultados(SemResultadosException e, WebRequest request) {
		return handleException(ResponseAndExceptionEnum.SEM_RESULTADOS, new Object[] { e.getMessage() }, e, request, HttpStatus.NOT_FOUND);
	}
	
	private ResponseEntity<Object> handleException(ResponseAndExceptionEnum exceptionEnum, Object[] params, Exception e, WebRequest request, HttpStatus status) {
		return handleException(exceptionEnum.getCode(), exceptionEnum.getMessageKey(), params, e, request, status);
	}
	
	private ResponseEntity<Object> handleException(Integer codigo, String messageKey, Object[] params, Exception e, WebRequest request, HttpStatus status) {
		logger.info(ExceptionUtils.getStackTrace(e));
		String message = messageSource.getMessage(messageKey, params, Locale.getDefault());
		Erro apiErro = new Erro(status);
		apiErro.setMessage(message);
		apiErro.setCodigo(codigo);
		return buildResponseEntity(apiErro);
	}
	
	
	private ResponseEntity<Object> buildResponseEntity(Erro err) {
		return new ResponseEntity<>(err, err.getStatus());
	}
	
}