package br.com.hoteis.api.repository;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.hoteis.api.model.HotelCidade;

@FeignClient(url="https://cvcbackendhotel.herokuapp.com/hotels/avail", name = "cidade")
public interface ViagemFeingRepository {
	
	@GetMapping("/{idCidade}")
    List<HotelCidade> valorHoteisCidade(@PathVariable("idCidade") String idCidade);
	
}
