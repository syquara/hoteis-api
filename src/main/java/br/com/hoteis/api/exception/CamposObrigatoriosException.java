package br.com.hoteis.api.exception;

public class CamposObrigatoriosException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CamposObrigatoriosException (String message) {
		super(message);
	}
	
	
}
