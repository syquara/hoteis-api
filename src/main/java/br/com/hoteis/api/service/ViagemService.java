package br.com.hoteis.api.service;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpClientErrorException;

import br.com.hoteis.api.exception.CamposObrigatoriosException;
import br.com.hoteis.api.exception.ErroInternoException;
import br.com.hoteis.api.exception.SemResultadosException;
import br.com.hoteis.api.model.HotelCidade;
import br.com.hoteis.api.model.PriceDetail;
import br.com.hoteis.api.model.Room;
import br.com.hoteis.api.repository.ViagemFeingRepository;
import br.com.hoteis.api.repository.ViagemRestTemplateRepository;
import br.com.hoteis.api.util.DateUtils;

@Service
public class ViagemService {

	Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private ViagemFeingRepository viagemFeingRepository;
	
	@Autowired
	private ViagemRestTemplateRepository viagemRestTemplateRepository;
	
	@Value("${execucao.quantidadeThread}")
	private int quantidadeThread;
	
	private final double comissao = 0.7;
	
	private DecimalFormat df = decimalFormat();

	public List<HotelCidade> valorHoteisCidade (String idCidade, String checkin, String checkout, Integer quantidadeAdultos, Integer quantidadeCriancas) {
		
		validaParemetrosEntrada (checkin, checkout, quantidadeAdultos, quantidadeCriancas);
		
		List<HotelCidade> valorHoteisCidade = null;
		
		try {
			
			valorHoteisCidade = viagemFeingRepository.valorHoteisCidade(idCidade);
			
			if(null == valorHoteisCidade || valorHoteisCidade.isEmpty()) {
				throw new SemResultadosException();
			}
			
			long diasEstadia = DateUtils.calculaDias(checkin, checkout);

			processaHotel(quantidadeAdultos, quantidadeCriancas, diasEstadia, valorHoteisCidade); 
			
		} catch (Exception e) {
			throw new ErroInternoException();
		}
		
		return valorHoteisCidade;
		
	}
	
	private void processaHotel(Integer quantidadeAdultos, Integer quantidadeCriancas, long diasEstadia, List<HotelCidade> valorHoteisCidade) {
		
		try {

            ExecutorService executorHotelCidade = Executors.newFixedThreadPool(quantidadeThread);
            valorHoteisCidade.forEach(hotelCidade -> {
                executorHotelCidade.execute(
                        new Runnable() {
                            @Override
                            public void run() {
                            	hotelCidade.setName(null);
                    			hotelCidade.setCityCode(null);
                    			processaRoom(quantidadeAdultos, quantidadeCriancas, diasEstadia, hotelCidade);
                            }
                        }
                );
            });

            executorHotelCidade.shutdown();

            while (!executorHotelCidade.isTerminated()) {
                try {
                    TimeUnit.SECONDS.sleep(3);
                } catch (InterruptedException ex) {
                    logger.error("Erro:", ex);
                }
            }

        } catch (Exception e) {
            logger.error("Erro: ", e);
        }
	}

	private void processaRoom(Integer quantidadeAdultos, Integer quantidadeCriancas, long diasEstadia, HotelCidade hotelCidade) {
		for (Room room : hotelCidade.getRooms()) {
			double adult = Double.parseDouble(df.format((room.getPrice().getAdult() / comissao) + room.getPrice().getAdult()).replace(",", "."));
			double child = Double.parseDouble(df.format((room.getPrice().getChild() / comissao) + room.getPrice().getChild()).replace(",", "."));
			room.setPriceDetail(new PriceDetail(adult, child));
			double totalAdult = adult * quantidadeAdultos;
			double totalChild = child * quantidadeCriancas;
			double totalPrice = Double.parseDouble(df.format((totalAdult + totalChild) * diasEstadia).replace(",", "."));
			room.setTotalPrice(totalPrice);
			room.setPrice(null);
		}
	}
	
	private DecimalFormat decimalFormat() {
		DecimalFormat df = new DecimalFormat("0.00");
		df.setRoundingMode(RoundingMode.HALF_UP);
		return df;
	}

	private void validaParemetrosEntrada(String checkin, String checkout, Integer quantidadeAdultos, Integer quantidadeCriancas) {
		
		if(!StringUtils.hasText(checkin) || !StringUtils.hasText(checkout) 
				|| null == quantidadeAdultos || quantidadeAdultos < 0 || null == quantidadeCriancas || quantidadeCriancas < 0) {
			throw new CamposObrigatoriosException("checkin, checkout, quantidadeAdultos, quantidadeCriancas");
		}
	}
	
	public List<HotelCidade> valorHotel (String idHotel, String checkin, 
			String checkout, Integer quantidadeAdultos, Integer quantidadeCriancas) throws ErroInternoException {

		validaParemetrosEntrada(checkin, checkout, quantidadeAdultos, quantidadeCriancas);
		
		List<HotelCidade> hotelCidade = null;

		try {
			
			hotelCidade = viagemRestTemplateRepository.valorHotel(idHotel);
			
			if(null == hotelCidade || hotelCidade.isEmpty()) {
				throw new SemResultadosException();
			}
			
			long diasEstadia = DateUtils.calculaDias(checkin, checkout);
			
			processaHotel(quantidadeAdultos, quantidadeCriancas, diasEstadia, hotelCidade);
			
		} catch (HttpClientErrorException e) {
			throw new ErroInternoException();
		}

		return hotelCidade;
	}

}
